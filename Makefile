NAME			= graphql-gqlgen-realtime-chat
ORCHESTRATOR	= docker compose
GOCMD			= go
BUILD_DIR		= build
BINARY_DIR		= $(BUILD_DIR)/bin
CODE_COVERAGE	= code-coverage

#TAG_COMMIT		= $(shell git rev-list --abbrev-commit --tags --max-count=1)
COMMIT			= $(shell git rev-parse --verify HEAD 2> /dev/null)
TAG_COMMIT		= $(shell git rev-list --abbrev-commit --tags --max-count=1 2> /dev/null)
TAG				= $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2> /dev/null || echo "0.0.0-$(COMMIT)")
# `2>/dev/null` suppress errors and `|| true` suppress the error codes.
#TAG 			= $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
# here we strip the version prefix
VERSION 		= $(TAG:v%=%)

.PHONY: all
all: download build

get_tag:
	@echo GIT_COMMIT=$(TAG)

.PHONY: container.start
container.start: ## Run project in docker environment
	@ $(ORCHESTRATOR) \
		-f compose.yml up -d --build --remove-orphans

.PHONY: container.ps
container.ps: ## List containers linked to this project
	@$(ORCHESTRATOR) -f compose.yml ps

.PHONY: container.stop
container.stop: ## Shutdown containers and remove volumes linked to this project
	@$(ORCHESTRATOR) -f compose.yml down

.PHONY: container.clean
container.clean: container.stop ## Delete image of this project
	@if [[ "$$(docker images -q $(NAME):latest 2> /dev/null)" != "" ]]; then \
		docker image rm $(NAME)-app; \
	fi

.PHONY: container.re
container.re: container.clean container.start

.PHONY: version
version: ## Display version project
	# check if the version string is empty
	@echo $(NAME)-$(VERSION)

${BINARY_DIR}:
	@mkdir -p $(BINARY_DIR)

.PHONY: download
download:
	@go mod download

.PHONY: build
build: ${BINARY_DIR} ## Compile the code, build Executable File
	@$(GOCMD) build -o $(BINARY_DIR)/app -v server.go

.PHONY: run
run: download ## Start application
	@ENVIRONMENT=development $(GOCMD) run server.go

.PHONY: clean
clean:  ## clean of project
	@if [[ -d $(BUILD_DIR) ]]; then \
		rm -rf $(BUILD_DIR); \
	fi

.PHONY: re
re: clean build  ## Remake project

.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
