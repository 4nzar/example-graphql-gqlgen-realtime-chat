package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/4nzar/example-graphql-gqlgen-realtime-chat/graph"
	"gitlab.com/4nzar/example-graphql-gqlgen-realtime-chat/graph/model"

	"github.com/rs/cors"

	"github.com/gorilla/websocket"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	// CORS setup
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:4000", "http://localhost:8080"},
		AllowCredentials: true,
		Debug:            false,
	})

	// fake db
	messages := []*model.Message{}

	// Use New instead of NewDefaultServer in order to have full control over defining transports
	srv := handler.New(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{
		ChatMessages:  messages,
		ChatObservers: map[string]chan []*model.Message{},
	}}))

	// Add Transport to support websocket for subscription
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	})

	// Activate introspection only in dev mode
	if os.Getenv("ENVIRONMENT") == "development" {
		srv.Use(extension.Introspection{})
	}

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", c.Handler(srv))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
