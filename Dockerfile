FROM golang:1.21-alpine AS build
RUN apk --no-cache add clang gcc g++ make git ca-certificates

WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o app .

FROM alpine
WORKDIR /usr/bin
COPY --from=build /app/app .
EXPOSE 8080
CMD ["app"]